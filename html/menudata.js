var menudata={children:[
{text:"Page principale",url:"index.html"},
{text:"Pages associées",url:"pages.html"},
{text:"Fichiers",url:"files.html",children:[
{text:"Liste des fichiers",url:"files.html"},
{text:"Variables globale",url:"globals.html",children:[
{text:"Tout",url:"globals.html",children:[
{text:"a",url:"globals.html#index_a"},
{text:"c",url:"globals.html#index_c"},
{text:"d",url:"globals.html#index_d"},
{text:"f",url:"globals.html#index_f"},
{text:"i",url:"globals.html#index_i"},
{text:"j",url:"globals.html#index_j"},
{text:"m",url:"globals.html#index_m"},
{text:"n",url:"globals.html#index_n"},
{text:"r",url:"globals.html#index_r"},
{text:"s",url:"globals.html#index_s"},
{text:"t",url:"globals.html#index_t"}]},
{text:"Fonctions",url:"globals_func.html"},
{text:"Variables",url:"globals_vars.html"},
{text:"Définitions de type",url:"globals_type.html"},
{text:"Macros",url:"globals_defs.html"}]}]}]}
