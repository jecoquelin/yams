# yams

Ce dépôt contient un jeu de yams avec un affichage console

## Contexte
Lors du BUT informatique, nous avons eu des projets à réaliser. Celui-ci est l'un deux.

## Description
Cette SAE a eu pour but de mettre nos connaissances de c en application en réalisant un jeu de yams. Cette SAE a affiné nos connaissances en langage C.

Étapes de cette SAE 1.01 :
1. Nous avons tout d'abord fait une maquette du jeu en format papier ou en format texte.
2. Nous avons ensuite réalisé un cahier de test fait avec libre office calc.
3. Nous avons réfléchi aux différentes fonctions que l'algorithme aurait et nous les avons décomposées.
4. Nous sommes passés à la programmation en langage c.
5. Nous avons pris des captures d'écran sur les différents tests qui fonctionnait en rapport avec le cahier de test.

## Apprentissage critique
AC 11.01 Implémenter des conceptions simples<br>
AC 11.03 Faire des essais et évaluer leurs résultats en regard des spécifications

## Langage et Framework 
  * C

## Prérequis 
Afin de pouvoir exécuter l'application sur votre poste, vous devrez avoir :
  * Gcc

## Exécution
Pour lancer le site, il vous suffit de compiler le code source et de le lancer à l'aide de cette commande `gcc -W -Wall -o yams.exe yams.c`.
