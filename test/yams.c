/**
 * \page Général
 * \author COQUELIN Jérémy (jeremy.coquelin@etudiant.univ-rennes1.fr)
 * \brief Programme permattant de jouer au jeu de Yams
 * \version 1.0
 * \date 14 septembre 2021
 * 
 * Ce programme permet à deux joueurs de jouer au jeu de Yams avec un affichage en mode console.
 *   
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <stdbool.h>
/**
 * \brief constante entier qui a la valeur -1.
 * 
 */
const int FIN=-1;

/**
 * \def N
 * 
 * \brief Taille maximale d'un tableau.
 * 
 * Taille maximale d'un tableau qu'on utilise pour les dés.
 */

#define N 5

/**
 * \typedef tableau
 * 
 * \brief Type tableau de N entier.
 * 
 * Le tableau permet de stocker jusqu'a 5 enter qui correspond aux 5 dé qui sont lancé.
 */
typedef int tableau[N];

/**
 * \def J
 * 
 * \brief Taille maximale d'un tableau.
 * 
 */
#define J 17

/**
 * \typedef resultat
 * 
 * \brief Type tableau de J entier. 
 * 
 * Le tableau permet de stocker toutes les valeur de toutes les combinaisons possibles.
 */
typedef int resultat[J];

resultat resultat1;

/**
* \brief Les Dés (d1,d2,d3,d4,d5) sont des variables globales.
*/
int d1,d2,d3,d4,d5;

/**
 * \brief Les variables 'joueur' sont des variables globales 
 * 
 */
char joueur1[100],joueur2[100];

void nom();
void afficherMaquette1(resultat resultatj1);
void relance(resultat resultat1);
void afficherMaquette2(resultat resultatj2);
void afficherDe();
int aleatoire();
int afficherD1();
int afficherD2();
int afficherD3();
int afficherD4();
int afficherD5();
bool fdouble(tableau tab);
void choixCase(tableau tab,char *decision,resultat resultat1);
void tri_par_insertion(tableau tab);
void res1(tableau tab,int val,resultat resultat1);
void initialise(tableau tablo);
void initialisemoins1(tableau tablo);
void resChance(tableau tab,resultat resultat1);
void resBrelan(tableau tab,resultat resultat1);
void resYams(tableau tab,resultat resultat1);
void resCarre(tableau tab,resultat resultat1);
void resFull_House(tableau tab,resultat resultat1);
void resPetite_Suite(tableau tab,resultat resultat1);
void resGrande_Suite(tableau tab,resultat resultat1);
void score(resultat resultat1);


/**
 * \fn int main() 
 * \brief Programme principal.
 * \return Code de sortie du programme (0 : sortie normale).
 * Le programme principale sert à appeler les procédure créée pour pouvoir jouer au Yams avec 2 joueurs différents.
 */
int main(){
  srand(time(NULL));
  resultat resultatj1;
  resultat resultatj2;
  initialisemoins1(resultatj2);
  initialisemoins1(resultatj1);
  nom();
  int i;
  for (i=0;i<13;i++){
    d1=aleatoire();
    d2=aleatoire();
    d3=aleatoire();
    d4=aleatoire();
    d5=aleatoire();
    afficherMaquette1(resultatj1);
    afficherDe();
    relance(resultatj1);
    afficherMaquette1(resultatj1);

    d1=aleatoire();
    d2=aleatoire();
    d3=aleatoire();
    d4=aleatoire();
    d5=aleatoire();
    afficherMaquette2(resultatj2);
    afficherDe();
    relance(resultatj2);
    afficherMaquette2(resultatj2);
  }
  score(resultatj1);
  score(resultatj2);
  afficherMaquette1(resultatj1);
  afficherMaquette2(resultatj2);
  if (resultatj1[16]>resultatj2[16]){
    printf("Le joueur %s a gagné\n",joueur1);
  }
  else if (resultatj1[16]<resultatj2[16])
  {
    printf("Le joueur %s a gagné\n",joueur2);
  }
  else{
    printf("Les joueurs %s et %s sont à égalité",joueur1,joueur2);
  }
  
  

  return EXIT_SUCCESS;
}

/**
 * \fn void nom()
 * \brief Procéure qui demande aux utilisateur d'enter leurs nom.
 * Demande le nom aux joueur et verifie si les noms sont conformes aux conditions.
 */
void nom(){
  
  printf("Nom du premier joueur ?\n");
  fgets(joueur1,100,stdin);
  while (strlen(joueur1)>21){
    printf("Votre nom est trop long . Retaper votre nom joueur 1\n");
    fgets(joueur1,100,stdin);
  }
  printf("Nom du deuxieme joueur ?\n");
  fgets(joueur2,100,stdin);
  while (strlen(joueur2)>21){
    printf("Votre nom est trop long . Retaper votre nom joueur 2\n");
    fgets(joueur2,100,stdin);
  }
  
  while (strcmp(joueur1, joueur2)==0 || (joueur1[0]=='\n') ||(joueur2[0]=='\n')|| joueur1[0]==' '||joueur2[0]==' '){ 
    if (strcmp(joueur1, joueur2)==0){
      printf("Il faut des noms différents pour jouer\n");
      printf("Nom du premier joueur ?\n");
      fgets(joueur1, 20,stdin);
      printf("Nom du deuxieme joueur ?\n");
      fgets(joueur2, 20,stdin);
    }
    else if ((joueur1[0]=='\n') || (joueur2[0]=='\n')){
      printf("Nom invalide. Veuillez les retaper.\n");
      printf("Nom du premier joueur ?\n");
      fgets(joueur1, 20,stdin);
      printf("Nom du deuxieme joueur ?\n");
      fgets(joueur2, 20,stdin);
    }
    else{
      printf("un des deux noms sont commence par un espace , veuillez les retaper\n");
      printf("Nom du premier joueur ?\n");
      fgets(joueur1, 20,stdin);
      printf("Nom du deuxieme joueur ?\n");
      fgets(joueur2, 20,stdin);
    }
  }
}

/**
 * \fn void afficherMaquette1(resultat resultatj1)
 * \brief affiche la maquette de jeu du joueur 1.
 * \param resultatj1 : paramètre d'entré qui permet de faire afficher les valeurs dans la maquette.
 */
void afficherMaquette1(resultat resultatj1){
  printf("Le joueur 1 est %s\n",joueur1);
  printf("___________________________\n");
  if (resultatj1[0]!=-1){
        printf("|1[totalde 1]       | %3d |\n",resultatj1[0]);
  }
  else{
    printf("|1[totalde 1]       |     |\n");
  }
  printf("|-------------------|-----|\n");
  if (resultatj1[1]!=-1){
    printf("|2[totalde 2]       | %3d |\n",resultatj1[1]);
  }
  else{
    printf("|2[totalde 2]       |     |\n");
  }
  printf("|-------------------|-----|\n");
  if (resultatj1[2]!=-1){
    printf("|3[totalde 3]       | %3d |\n",resultatj1[2]);
  }
  else{
    printf("|3[totalde 3]       |     |\n");
  }
  printf("|-------------------|-----|\n");
  if (resultatj1[3]!=-1){
    printf("|4[totalde 4]       | %3d |\n",resultatj1[3]);
  }
  else{
    printf("|4[totalde 4]       |     |\n");
  }
  printf("|-------------------|-----|\n");
  if (resultatj1[4]!=-1){
    printf("|5[totalde 5]       | %3d |\n",resultatj1[4]);
  }
  else{
    printf("|5[totalde 5]       |     |\n");
  }
  printf("|-------------------|-----|\n");
  if (resultatj1[5]!=-1){
    printf("|6[totalde 6]       | %3d |\n",resultatj1[5]);
  }
  else{
    printf("|6[totalde 6]       |     |\n");
  }
  printf("|-------------------|-----|\n");
  if (resultatj1[6]!=-1){
    printf("|Bonus si > à 62[35]| %3d |\n",resultatj1[6]);
  }
  else{
    printf("|Bonus si > à 62[35]|     |\n");
  }
  printf("|-------------------|-----|\n");
  if (resultatj1[7]!=-1){
    printf("|Total supérieur    | %3d |\n",resultatj1[7]);
  }
  else{
    printf("|Total supérieur    |     |\n");
  }
  printf("|_________________________|\n");
  printf("___________________________\n");
  if (resultatj1[8]!=-1){
    printf("|Brelan[total]      | %3d |\n",resultatj1[8]);
  }
  else{
    printf("|Brelan[total]      |     |\n");
  }
  printf("|-------------------|-----|\n");
  if (resultatj1[9]!=-1){
    printf("|Carré[total]       | %3d |\n",resultatj1[9]);
  }
  else{
    printf("|Carré[total]       |     |\n");
  }
  printf("|-------------------|-----|\n");
  if (resultatj1[10]!=-1){
    printf("|Full House[25]	    | %3d |\n",resultatj1[10]);
  }
  else{
    printf("|Full House[25]	    |     |\n");
  }
  printf("|-------------------|-----|\n");
  if (resultatj1[11]!=-1){
    printf("|Petite suite[30]   | %3d |\n",resultatj1[11]);
  }
  else{
    printf("|Petite suite[30]   |     |\n");
  }
  printf("|-------------------|-----|\n");
  if (resultatj1[12]!=-1){
    printf("|Grande suite[40]   | %3d |\n",resultatj1[12]);
  }
  else{
    printf("|Grande suite[40]   |     |\n");
  }
  printf("|-------------------|-----|\n");
  if (resultatj1[13]!=-1){
    printf("|Yams[50]           | %3d |\n",resultatj1[13]);
  }
  else{
    printf("|Yams[50]           |     |\n");
  }
  printf("|-------------------|-----|\n");
  if (resultatj1[14]!=-1){
    printf("|Chance[total]      | %3d |\n",resultatj1[14]);
  }
  else{
    printf("|Chance[total]      |     |\n");
  }
  printf("|-------------------|-----|\n");
  if (resultatj1[15]!=-1){
    printf("|Total inférieur    | %3d |\n",resultatj1[15]);
  }
  else{
    printf("|Total inférieur    |     |\n");
  }
  printf("|___________________|_____|\n");
  printf("___________________________\n");
  if (resultatj1[16]!=-1){
    printf("|Total              | %3d |\n",resultatj1[16]);
  }
  else{
    printf("|Total              |     |\n");
  }
  printf("|___________________|_____|\n");
  printf("\n");
}

/**
 * \fn void afficherMaquette2(resultat resultatj2)
 * \brief affiche la maquette de jeu du joueur 2.
 * 
 * \param resultatj2 : paramètre d'entré qui permet de faire afficher les valeurs dans la maquette.
 */
void afficherMaquette2(resultat resultatj2){
  printf("Le joueur 2 est %s\n",joueur1);
  printf("___________________________\n");
  if (resultatj2[0]!=-1){
        printf("|1[totalde 1]       | %3d |\n",resultatj2[0]);
  }
  else{
    printf("|1[totalde 1]       |     |\n");
  }
  printf("|-------------------|-----|\n");
  if (resultatj2[1]!=-1){
    printf("|2[totalde 2]       | %3d |\n",resultatj2[1]);
  }
  else{
    printf("|2[totalde 2]       |     |\n");
  }
  printf("|-------------------|-----|\n");
  if (resultatj2[2]!=-1){
    printf("|3[totalde 3]       | %3d |\n",resultatj2[2]);
  }
  else{
    printf("|3[totalde 3]       |     |\n");
  }
  printf("|-------------------|-----|\n");
  if (resultatj2[3]!=-1){
    printf("|4[totalde 4]       | %3d |\n",resultatj2[3]);
  }
  else{
    printf("|4[totalde 4]       |     |\n");
  }
  printf("|-------------------|-----|\n");
  if (resultatj2[4]!=-1){
    printf("|5[totalde 5]       | %3d |\n",resultatj2[4]);
  }
  else{
    printf("|5[totalde 5]       |     |\n");
  }
  printf("|-------------------|-----|\n");
  if (resultatj2[5]!=-1){
    printf("|6[totalde 6]       | %3d |\n",resultatj2[5]);
  }
  else{
    printf("|6[totalde 6]       |     |\n");
  }
  printf("|-------------------|-----|\n");
  if (resultatj2[6]!=-1){
    printf("|Bonus si > à 62[35]| %3d |\n",resultatj2[6]);
  }
  else{
    printf("|Bonus si > à 62[35]|     |\n");
  }
  printf("|-------------------|-----|\n");
  if (resultatj2[7]!=-1){
    printf("|Total supérieur    | %3d |\n",resultatj2[7]);
  }
  else{
    printf("|Total supérieur    |     |\n");
  }
  printf("|_________________________|\n");
  printf("___________________________\n");
  if (resultatj2[8]!=-1){
    printf("|Brelan[total]      | %3d |\n",resultatj2[8]);
  }
  else{
    printf("|Brelan[total]      |     |\n");
  }
  printf("|-------------------|-----|\n");
  if (resultatj2[9]!=-1){
    printf("|Carré[total]       | %3d |\n",resultatj2[9]);
  }
  else{
    printf("|Carré[total]       |     |\n");
  }
  printf("|-------------------|-----|\n");
  if (resultatj2[10]!=-1){
    printf("|Full House[25]	    | %3d |\n",resultatj2[10]);
  }
  else{
    printf("|Full House[25]	    |     |\n");
  }
  printf("|-------------------|-----|\n");
  if (resultatj2[11]!=-1){
    printf("|Petite suite[30]   | %3d |\n",resultatj2[11]);
  }
  else{
    printf("|Petite suite[30]   |     |\n");
  }
  printf("|-------------------|-----|\n");
  if (resultatj2[12]!=-1){
    printf("|Grande suite[40]   | %3d |\n",resultatj2[12]);
  }
  else{
    printf("|Grande suite[40]   |     |\n");
  }
  printf("|-------------------|-----|\n");
  if (resultatj2[13]!=-1){
    printf("|Yams[50]           | %3d |\n",resultatj2[13]);
  }
  else{
    printf("|Yams[50]           |     |\n");
  }
  printf("|-------------------|-----|\n");
  if (resultatj2[14]!=-1){
    printf("|Chance[total]      | %3d |\n",resultatj2[14]);
  }
  else{
    printf("|Chance[total]      |     |\n");
  }
  printf("|-------------------|-----|\n");
  if (resultatj2[15]!=-1){
    printf("|Total inférieur    | %3d |\n",resultatj2[15]);
  }
  else{
    printf("|Total inférieur    |     |\n");
  }
  printf("|___________________|_____|\n");
  printf("___________________________\n");
  if (resultatj2[16]!=-1){
    printf("|Total              | %3d |\n",resultatj2[16]);
  }
  else{
    printf("|Total              |     |\n");
  }
  printf("|___________________|_____|\n");
}

/**
 * \fn void afficherDe()
 * \brief affiche les dés qui ont été tiré au hasard par le programme
 *
 */
void afficherDe(){
  printf("-----    -----    -----    -----    -----\n| %d |    | %d |    | %d |    | %d |    | %d |\n-----    -----    -----    -----    -----\n",d1,d2,d3,d4,d5);
  ;
}

/**
 * \fn int afficherD1()
 * 
 * \brief Cette fonction permet d'être indépendante et de retourner le premier dé.
 * 
 * \return un entier qui est le premier dé qui a été changé.
 */
int afficherD1(){
  d1=aleatoire();
  return d1;
}

/**
 * \fn int afficherD2()
 * 
 * \brief Cette fonction permet d'être indépendante et de retourner le deuxième dé.
 * 
 * \return un entier qui est le deuxième dé qui a été changé.
 */
int afficherD2(){
  d2=aleatoire();
  return d2;
}

/**
 * \fn int afficherD3()
 * 
 * \brief Cette fonction permet d'être indépendante et de retourner le troisième dé.
 * 
 * \return un entier qui est le troisième dé qui a été changé.
 */
int afficherD3(){
  d3=aleatoire();
  return d3;
}

/**
 * \fn int afficherD4()
 * 
 * \brief Cette fonction permet d'être indépendante et de retourner le quatrième dé.
 * 
 * \return un entier qui est le quatrième dé qui a été changé.
 */
int afficherD4(){
  d4=aleatoire();
  return d4;
}

/**
 * \fn int afficherD5()
 * 
 * \brief Cette fonction permet d'être indépendante et de retourner le cinquième dé.
 * 
 * \return un entier qui est le cinquième dé qui a été changé.
 */
int afficherD5(){
  d5=aleatoire();
  return d5;
}

/**
 * \fn int aleatoire()
 * \brief Génère un nombre aléatoire entre 1 et 6.
 * 
 * \return un entier.
 *  
 * Cette fonction retourne un nombre aléatoire défini entre 1 et 6 tout deux compris.
 */
int aleatoire(){
  int res;
  res=rand() % 6 + 1;
  return res ;
}

/**
 * \fn void initialise(tableau tablo)
 * \brief Procédure qui permet d'initialiser toutes les valeur a '\0' d'un tableau.
 * 
 * \param tablo : paramètre d'entrée/sortie qui modifie les valeurs d'un tablo qui représentes les dés.  
 */
void initialise(tableau tablo){
  int i;
  for (i=0;i<N;i++){
    tablo[i]=-1;
  } 
}

/**
 * \fn void initialisemoins1(tableau tablo)
 * \brief initialise toutes les valeurs d'un tableau à -1.
 * 
 * \param tablo : paramètre d'entrée/sortie qui modifie les valeurs d'un tablo qui représentes le tableau des scores.
 */
void initialisemoins1(tableau tablo){
  int i;
  for (i=0;i<J;i++){
    tablo[i]=-1;
  } 
}

/**
 * \fn void relance(resultat resultat1)
 * \brief permet de relancer les dés si l'utilisateur en a envie.
 * \param resultat1 : paramètre d'entrée et de sortie qui représente la tableau des scores.
 * 
 */
void relance(resultat resultat1){
  char poubelle;
  char decision;
  int i,j,x;// i est un compteur , j est pour un indice de tableau et x est un compteur.
  bool a; // a est un booléen ui vas permettre dans la suite du programme de stocker true si ,dans le tableau de ralnce des dés, il y a un double, false sinon. 
  i=0;
  j=0;
  int choix; 
  tableau tab;
  initialise(tab);
  int de_relance;
  printf("Voulez vous relancer les dés ?(Y pour oui ou N pour non)\n");
  scanf("%c",&decision);
  scanf("%c",&poubelle);
  
  while (decision=='Y' && i<2){
  
      printf("Il faut appuyer sur -1 pour arrêter de sélectionner les dés a relancé\n");
      scanf("%d",&de_relance);
      while (de_relance!=-1 &&de_relance!=1&& de_relance!=2&& de_relance!=3 && de_relance!=4 && de_relance!=5){
        printf("Veuillez entrer un réponse valide\n");
        scanf("%d",&de_relance);
      }
      while ((de_relance!=-1) && de_relance>=1&& de_relance<=5){
        
        tab[j]=de_relance;
        j++;
        scanf("%d",&de_relance);
      }
      tri_par_insertion(tab);
      
      
      
      a=fdouble(tab);
      while (a==true){
        initialise(tab);
        printf("Vous avez entrez des dés identiques, Veuillez les ressaisir.Il faut appuyer sur -1 pour arrêter de sélectionner les dés a relancé\n");
        scanf("%d",&de_relance);
        j=0;
        while ((de_relance!=-1) && de_relance>=1&& de_relance<=5){
          tab[j]=de_relance;
          j++;
          scanf("%d",&de_relance);
        }
        a=fdouble(tab);
        
      }
      
      for (x=1;x<N;x++){
      choix=0;
      choix=tab[x];
      
        switch (choix)
        {
        case (1):
          afficherD1();
          break;
        case (2):
          afficherD2();
          break;
        case (3):
          afficherD3();
          break;
        case (4):
          afficherD4();
          break;
        case (5):
          afficherD5();
          break;
        default:
          break;
      }
      }
    afficherDe();
    initialise(tab);
    
    i++;
  }
  tri_par_insertion(tab);
  tableau tDes={d1,d2,d3,d4,d5};
  
  choixCase(tDes,&decision,resultat1);
}

/**
 * \fn bool fdouble(tableau tab)
 * \brief Fonction qui verifie si deux nombre sont identiques dans un tableau.
 * 
 * \param tab : paramètre d'entré qui représente le tableau des dés que l'utilisateur veut relancer. 
 * \return true si il y a deux valeurs identiques dans un tableau et false quand il n'y a pas deux valeurs identiques. 
 */
bool fdouble(tableau tab){
  bool trouve;
  trouve =false;
  int i=0;
  
  while ((i<N))
  {
    if (tab[i]==tab[i+1] && tab[i]!=-1){
      trouve=true;
    }
    i++;
  }
  return trouve;
}

/**
 * \fn void choixCase(tableau tDes,char *decision,resultat resultat1)
 * \brief demande a l'utilisateur dans quel case il veut mettre son score.
 * 
 * \param tab : paramètre d'entré qui représente le tableau ou sont stocker les dés
 * \param decision : paramètre d'entré qui rerésente la décision de la fonction relance pour stocker la variable choix.
 * \param resultat1 : paramètre d'entrée/sortie qui représente le tableau ou vont être stocker les scores.
 */
void choixCase(tableau tDes,char *decision,resultat resultat1){
  char choix,poubelle; // poubelle est une variable qui nous sert a se débarraser du retour chariot.
  int val;
  tri_par_insertion(tDes);
  printf("Dans quel case voulez vous mettre votre score ?\n");
  printf("1 pour total de 1\n2 pour total de 2\n3 pour total de 3\n4 pour total de 4\n5 pour total de 5\n6 pour total de 6\nb pour Brelan\nc pour Carré\nf pour Full house\np pour Petite suite\ng pour Grande suite\ny pour Yams\nl pour Chance\n");
  
  if (*decision=='Y'){
    scanf("%c%c%c",&poubelle,&choix,&poubelle);   // Cette itération permet d'affecter une valeur à la variable choix                                                   
  }                                               // car suivant ce qu'on a décider pour la relance des dés, il y a plus                                                   
  else{                                           // ou moins de retour chariot.
    scanf("%c%c",&choix,&poubelle);
  }

  val=atoi(&choix); //atoi permet de convertir une chaine de caractère dans un entier.
  switch (choix)
    {
    case ('1'):res1(tDes,val,resultat1);break;
    case ('2'):res1(tDes,val,resultat1);break;
    case ('3'):res1(tDes,val,resultat1);break;
    case ('4'):res1(tDes,val,resultat1);break;
    case ('5'):res1(tDes,val,resultat1);break;
    case ('6'):res1(tDes,val,resultat1);break;
    case ('b'):resBrelan(tDes,resultat1);break;
    case ('c'):resCarre(tDes,resultat1);break;
    case ('f'):resFull_House(tDes,resultat1);break;
    case ('p'):resPetite_Suite(tDes,resultat1);break;
    case ('y'):resYams(tDes,resultat1);break;
    case ('l'):resChance(tDes,resultat1);break;
    default:break;
    }
}

/**
 * \fn void tri_par_insertion(tableau tab)
 * \brief trie le tableau en mode croissant.
 * \param tab : paramètre d'entrée/sortie qui représentes le tableau de dés trié.
 */
void tri_par_insertion(tableau tab){
  int i, j, tmp;
  for (i=1 ; i <= N-1; i++) {
    j = i;
 
    while (j > 0 && tab[j-1] > tab[j]) {
      tmp = tab[j];
      tab[j] = tab[j-1];
      tab[j-1] = tmp;
      j--;
    }
  }
}

/**
 * \fn void res1(tableau tab,int val,resultat resultat1)
 * 
 *
 * \brief Procédure qui permet de calculer le résultat de la première/deuxième case.
 * 
 * \param tab : Paramètre d'entré qui représente les dés
 * \param val : Paramètre d'entré qui permet de savoir quel est la valeur de l'entier qu'as choisi l'utilisateur quand c'est 1 à 6.
 * \param resultat1 : Paramètre d'entré/sortie qui représente le tableau des score.
 */
void res1(tableau tDes,int val,resultat resultat1){
  int i,compter,res;
  i=0;
  compter=0;
  tri_par_insertion(tDes);
  while (i<N) 
  {
    if (tDes[i]==val){
      compter++;
    }
    i++;
  }
  res=compter*val;
  switch(val){
    case (1):resultat1[0]=res;break;
    case (2):resultat1[1]=res;break;
    case (3):resultat1[2]=res;break;
    case (4):resultat1[3]=res;break;
    case (5):resultat1[4]=res;break;
    case (6):resultat1[5]=res;break;
  }
}

/**
 * \fn void resChance(tableau tab,resultat resultat1)
 * \brief Procédure qui permet de calculer la somme des dés pour mettre le score dans un tableau dans la case Chance.
 * 
 * \param tab : paramètre d'entré qui représente un tableau ou sont stocker a l'intérieur des dés.
 * \param resultat1 : paramètre d'entrée/sortie qui représente un tableau ou sont stocker les scores.
 */
void resChance(tableau tab,resultat resultat1){
  int i,somme;
  somme=0;
  for (i=0;i<N;i++){
    somme =somme + tab[i];
  }
  resultat1[14]=somme;
}

/**
 * \fn void resBrelan(tableau tab,resultat resultat1)
 * \brief Procédure qui permet de calculer la somme des dés pour mettre le score dans un tableau dans la case Brelan. Cependant il faut 3 dés de même valeurs pour mettre les somme des dés sinon si il repecte pas la condition, la valeur mise sera 0.
 * 
 * \param tab : paramètre d'entré qui représente un tableau où sont stocker à l'intérieur des dés.
 * \param resultat1 : paramètre d'entrée/sortie qui représente un tableau ou sont stocker les scores.
 */
void resBrelan(tableau tab,resultat resultat1){
  int i=0;
  int j;
  int somme=0;
  while (i<N-2)
  {
    if (tab[i]==tab[i+1] && tab[i+1]==tab[i+2] && tab[i]!=0){
      for (j=0;j<N;j++){
        somme =somme + tab[j];
        }
    }
  i++;  
  }
  resultat1[8]=somme;
  
}

/**
 * \fn void resCarre(tableau tab,resultat resultat1)
 * \brief Procédure qui permet de calculer la somme des dés pour mettre le score dans un tableau dans la case Carre. Cependant il faut 4 dés de même valeurs pour mettre la somme des dés. Si il repecte pas la condition, la valeur mise sera 0.
 * 
 * \param tab : paramètre d'entré qui représente un tableau où sont stocker à l'intérieur des dés.
 * \param resultat1 : paramètre d'entrée/sortie qui représente un tableau où sont stocker les scores.
 */
void resCarre(tableau tab,resultat resultat1){
  int i=0;
  int j;
  int somme=0;
  while (i<N-3)
  {
    if (tab[i]==tab[i+1]&& tab[i+1]==tab[i+2] && tab[i+2]==tab[i+3] && tab[i]!=0){
      for (j=0;j<N;j++){
        somme = somme + tab[j];
        }
    }
  i++;  
  }
  resultat1[9]=somme;
}

/**
 * \fn void resYams(tableau tab,resultat resultat1)
 * \brief Procédure qui permet de mettre le score 50 dans un tableau dans la case Yams. Cependant il faut 5 dés de même valeurs pour mettre le score dans le tableau . Si il repecte pas la condition, la valeur mise sera 0.
 * 
 * \param tab : paramètre d'entré qui représente un tableau où sont stocker à l'intérieur des dés.
 * \param resultat1 : paramètre d'entrée/sortie qui représente un tableau ou sont stocker les scores.
 */
void resYams(tableau tab,resultat resultat1){
  if (tab[1] == tab[2] && tab[2] == tab[3] &&  tab[3] == tab[4] && tab[4] == tab[5] ){
    resultat1[13]=50;
  }
  else{
    resultat1[13]=0;
  }
}

/**
 * \fn void resFull_House(tableau tab,resultat resultat1)
 * \brief Procédure qui permet de mettre le score 25 dans un tableau dans la case Full House. Cependant il faut un brelan et une paire pour mettre la somme des dés. Si il repecte pas la condition, la valeur mise sera 0.
 * 
 * \param tab : paramètre d'entré qui représente un tableau où sont stocker à l'intérieur des dés.
 * \param resultat1 : paramètre d'entrée/sortie qui représente un tableau où sont stocker les scores.
 */
void resFull_House(tableau tab,resultat resultat1){
  int x=0;
  int i=0;
  int verif_brelan=0;
  int verif_paire = 0;
  int j=0;
  int z;
  
  while (j<N-2)
  {
    if (tab[j]==tab[j+1] && tab[j+1]==tab[j+2] && tab[j]!=0){
      for (z=0;z<N;z++){
        verif_brelan=verif_brelan + tab[j];
        }
    }
  j++;  
  }
  x=verif_brelan;
  x=x/3;
  if (verif_brelan!=0){
    
    while ((tab[i]<=tab[N])&& (i<N-1)){
      
      if (tab[i]==tab[i+1] && tab[i]!=x && tab[i]!=0){
        
        verif_paire= tab[i]*2;
      }
    i++;
    }
  }
  if (verif_paire!=0 || verif_brelan!=0){
    resultat1[10]=25;
  }
  else {
    resultat1[10]=0;
  }
}

/**
 * \fn void resPetite_Suite(tableau tab,resultat resultat1)
 * \brief Procédure qui permet mettre le score 30 dans un tableau dans la case Petite suite. Cependant il faut 4 dé consécutif (ex:1,2,3,4) pour mettre la somme des dés. Si il repecte pas la condition, la valeur mise sera 0.
 * 
 * \param tab : paramètre d'entré qui représente un tableau où sont stocker à l'intérieur des dés.
 * \param resultat1 : paramètre d'entrée/sortie qui représente un tableau où sont stocker les scores.
 */
void resPetite_Suite(tableau tab,resultat resultat1){
  int i=0;
  int cpt=0;
  while (cpt<3 && i<N){
    if (tab[i]+1==tab[i+1]){
      cpt++;
    }
    i++;
  }
  if (cpt==3){
    resultat1[11]=30;
  }
  else{
    resultat1[11]=0;
  }
}

/**
 * \fn void resGrande_Suite(tableau tab,resultat resultat1)
 * \brief Procédure qui permet mettre le score 40 dans un tableau dans la case Grande suite. Cependant il faut 5 dé consécutif (ex:1,2,3,4,5) pour mettre la somme des dés. Si il repecte pas la condition, la valeur mise sera 0.
 * 
 * \param tab : paramètre d'entré qui représente un tableau où sont stocker à l'intérieur des dés.
 * \param resultat1 : paramètre d'entrée/sortie qui représente un tableau où sont stocker les scores.
 */
void resGrande_Suite(tableau tab,resultat resultat1){
  if ((tab[0]==2 && tab[1]==3 && tab[2]==4 && tab[3]==5 && tab[4]==6)||(tab[0]==1 && tab[1]==2 && tab[2]==3 && tab[3]==4 && tab[4]==5)){ //|| (tab[1]==0 && tab[2]==1 && tab[3]==1 && tab[4]==1 && tab[5]==1 && tab[6]==1)){
    resultat1[11]=30;
  }
  else {
    resultat1[11]=0;
  }
}

/**
 * \fn void score(resultat resultat1)
 * \brief calcule les scores finaux
 * 
 * \param resultat1 : paramètre d'entrée/sortie qui représente un tableau où sont stocker les scores.
 */
void score(resultat resultat1){
  int temp=0;
  temp= resultat1[0]+resultat1[1]+resultat1[2]+resultat1[3]+resultat1[4]+resultat1[5];
  if (temp>62){
    resultat1[6]=35;
  }
  else{
    resultat1[6]=0;
  }
  resultat1[7]=temp+resultat1[6];
  resultat1[15]=resultat1[8]+resultat1[9]+resultat1[10]+resultat1[11]+resultat1[12]+resultat1[13]+resultat1[14];
  resultat1[16]=resultat1[7]+resultat1[15];
}